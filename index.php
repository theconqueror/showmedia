<!doctype html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ShowMedia</title>	

	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
  <style type="text/css">
    .borderless td, .borderless th {
    border: none;
    }
  </style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#"><h3>ShowMedia</h3></a>

</nav>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
        <div class="panel-heading">
        <h4>Detail User</h4>
        </div>

        <div class="panel-body">
          <div class="col-md-12">
          <table class="table table-responsive">
            <tr>
              <td><img id="img-picture" src="" /></td>
              <td>
              <table class="table borderless">
                <tr>
                  <td>Username </td>
                  <td>: <span id="username"></span></td>
                <tr>
                  <td>Nama Lengkap </td>
                  <td>: <span id="fullname"></span></td>
                </tr>
                <tr>
                  <td>Biografi </td>
                  <td>: <span id="bio"></span></td>
                </tr>
              </table>
              </td>
            </tr>
          </table>
    </div>
        </div>
		</div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
        <h4>Recent Media Showed by User</h4>
        </div>

        <div class="panel-body">
          <div class="col-md-12">
          <table class="table table-responsive" id="example">
          </table>
    </div>
        </div>
    </div>
    </div>
    
	</div>
</div>

</body>
</html>
<script type="text/javascript" src="js/date-format.js"></script>
<script type="text/javascript">
  var urlBasic = "https://api.instagram.com/v1/users/self/?access_token=2023153920.2582478.f63b21426955449b804707687c08ec5b";
  
  var urlMedia = "https://api.instagram.com/v1/users/self/media/recent/?access_token=2023153920.2582478.f63b21426955449b804707687c08ec5b";

  $.ajax({
    url: urlBasic,
    method: 'GET',
    dataType: 'json',
    success: function(data) {
      var json = JSON.parse(JSON.stringify(data));

      $('#username').html(json['data'].username);
      $('#fullname').html(json['data'].full_name);
      $('#bio').html(json['data'].bio);
      $('#img-picture').attr('src', json['data'].profile_picture);
      

    },
    error: function(err) {
      console.log('error:' + err)
    }
}),

  $.ajax({
    url: urlMedia,
    method: 'GET',
    dataType: 'json',
    success: function(data) {
      var json = JSON.parse(JSON.stringify(data));
      var tableRow = "";
      var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
      var count = 0;


      $.each(json, function( index, value ) {
        $.each(data[index], function(x,y){
          var image = data[index][x];
          if(typeof image['images'] !== 'undefined'){
            console.log(image);
            if(count == 0){
              tableRow+="<tr>"
            }
            if(count < 4){
              tableRow +='<td><img src="'+ image['images']['thumbnail'].url + '"/><br/>' + Tanggal Post : +  dateFormat(Date(image['caption']['created_time']), "yyyy-mm-dd") + ' </td>';
              count++; 
            }else{
             tableRow +='<td><img src="'+ image['images']['thumbnail'].url + '"/><br/>' +  Tanggal Post : +  dateFormat(Date(image['caption']['created_time']), "yyyy-mm-dd") + ' </td></tr>';
             count=0;
            }
          }
        })
      });
      $('#example').html(tableRow);
      
    },
    error: function(err) {
      console.log('error:' + err)
    }
})
</script>